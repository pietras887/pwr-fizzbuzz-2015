﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTestProject6
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void When_1_Then_1()
        {
            FizzBuzz sut = new FizzBuzz();
            string result;

            result = sut.Parse(1);

            Assert.AreEqual("1", result);     
        }

        [TestMethod]
        public void When_3_Then_Fizz()
        {
            FizzBuzz sut = new FizzBuzz();
            string result;

            result = sut.Parse(3);

            Assert.AreEqual("Fizz", result);
        }
        [TestMethod]
        public void When_5_Then_Buzz()
        {
            FizzBuzz sut = new FizzBuzz();
            string result;

            result = sut.Parse(5);

            Assert.AreEqual("Buzz", result);
        }

        [TestMethod]
        public void When_2_Then_2()
        {
            FizzBuzz sut = new FizzBuzz();
            string result;

            result = sut.Parse(2);

            Assert.AreEqual("2", result);
        }
        [TestMethod]
        public void When_4_Then_4()
        {
            FizzBuzz sut = new FizzBuzz();
            string result;

            result = sut.Parse(4);

            Assert.AreEqual("4", result);
        }
        [TestMethod]
        public void When_15_Then_15()
        {
            FizzBuzz sut = new FizzBuzz();
            string result;

            result = sut.Parse(15);

            Assert.AreEqual("FizzBuzz", result);
        }
        [TestMethod]
        public void When_7_Then_Buzzinga()
        {
            FizzBuzz sut = new FizzBuzz();
            string result;

            result = sut.Parse(7);

            Assert.AreEqual("Buzzinga", result);
        }
        [TestMethod]
        public void When_21_Then_FizzBuzzinga()
        {
            FizzBuzz sut = new FizzBuzz();
            string result;

            result = sut.Parse(21);

            Assert.AreEqual("FizzBuzzinga", result);
        }
        [TestMethod]
        public void When_13_Then_Fizz()
        {
            FizzBuzz sut = new FizzBuzz();
            string result;

            result = sut.Parse(13);

            Assert.AreEqual("Fizz", result);
        }
        [TestMethod]
        public void When_35_Then_FizzBuzzBuzzinga()
        {
            FizzBuzz sut = new FizzBuzz();
            string result;

            result = sut.Parse(35);

            Assert.AreEqual("FizzBuzzBuzzinga", result);
        }
        [TestMethod]
        public void When_51_Then_FizzBuzz()
        {
            FizzBuzz sut = new FizzBuzz();
            string result;

            result = sut.Parse(51);

            Assert.AreEqual("FizzBuzz", result);
        }
        [TestMethod]
        public void When_0_Then_0()
        {
            FizzBuzz sut = new FizzBuzz();
            string result;

            result = sut.Parse(0);

            Assert.AreEqual("0", result);
        }

        [TestMethod]
        public void When_123_Then_12Fizz()
        {
            FizzBuzz sut = new FizzBuzz();
            string result;

            int[] t = {1,2,3};
            result = sut.Parse2(t);

            Assert.AreEqual("12Fizz", result);
        }
    }

}
