﻿namespace UnitTestProject6
{
    public class FizzBuzz
    {
        public string Parse(int x)
        {
            string zwrot = "";

            if (x == 0)
            {
                return "0";
            }
            if (x.ToString().Contains("3") || x % 3 == 0)
            {
                zwrot += "Fizz";
            }
            if (x.ToString().Contains("5") || x % 5 == 0)
            {
                zwrot += "Buzz";
            }
            if (x % 7 == 0)
            {
                zwrot += "Buzzinga";
            }
            if (zwrot.Length == 0)
            {
                zwrot = x.ToString();
            }
            return zwrot;
        }
        public string Parse2(int[] obiekt)
        {
            string zwrot = "";

            for (int i = 0; i < obiekt.Length; ++i)
                zwrot += Parse(obiekt[i]);

            return zwrot;
        }
    }
}
